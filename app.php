<?php

require_once __DIR__ . '/vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FingersCrossedHandler;

// create a log channel
$logger = new Logger('logger');
$fingersCrossedHandler = new FingersCrossedHandler(new StreamHandler('php://stderr'));
$logger->pushHandler($fingersCrossedHandler);
// $logger->pushHandler(new StreamHandler('php://stderr'));

$count = 1;
$criticalAt = getenv('CRITICAL_AT') ?: 3600;
while (true) {
    $memory = memory_get_peak_usage(true)/1024/1024;
    $msg = "This is the $count loop. Memory usage: $memory MB.";
    if ($count % $criticalAt == 0) {
        $logger->critical($msg);
        $fingersCrossedHandler->reset();
    } else {
        $logger->info($msg, $_SERVER);
    }
    sleep(1);
    $count++;
}
